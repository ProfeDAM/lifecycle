package com.example.profedam.lifecycle;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends Activity {

    private String txtHora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notify("onCreate");
        setContentView(R.layout.activity_main);
        txtHora = getDate();

        TextView tvHora = (TextView) findViewById(R.id.tvhora);
        tvHora.setText (this.txtHora);



    }

    private String getDate() {
        /* Aquest mètode retorna en format String la data hora del sistema */
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        return sdf.format(new Date());
    }



    @Override
    protected void onPause() {
        super.onPause();
        notify("onPause");
    }


    @Override
    protected void onResume() {
        super.onResume();
        notify("onResume");
    }


    @Override
    protected void onStart() {
        super.onStart();
        notify("onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        notify("onRestart");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        notify ("onDestroy");
    }

    @Override
    protected void onStop() {


        super.onStop();
        notify("onStop");
    }



    public void notify (String mensaje)
    {
        System.out.println (mensaje);
        Log.i("info", mensaje);
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();

    }

}
